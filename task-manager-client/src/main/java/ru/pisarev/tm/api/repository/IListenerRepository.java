package ru.pisarev.tm.api.repository;

import ru.pisarev.tm.listener.AbstractListener;

import java.util.Collection;

public interface IListenerRepository {


    Collection<AbstractListener> getListeners();

    Collection<AbstractListener> getArguments();

    Collection<String> getListenerNames();

    Collection<String> getListenerArg();

    AbstractListener getListenerByName(String name);

    AbstractListener getListenerByArg(String arg);

    void add(AbstractListener listener);
}