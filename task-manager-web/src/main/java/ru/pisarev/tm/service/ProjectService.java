package ru.pisarev.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pisarev.tm.api.service.IProjectService;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.repository.IProjectRepository;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ProjectService extends AbstractRecordService<Project> implements IProjectService {

    @NotNull
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<Project> collection) {
        if (collection == null) return;
        for (Project item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    public Project add(@Nullable final Project entity) {
        if (entity == null) return null;
        repository.save(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public Project findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new)).orElse(null);
    }

    @Override
    @SneakyThrows
    public void clear() {
        repository.deleteAll();
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        repository.deleteById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Project entity) {
        if (entity == null) return;
        repository.deleteById(entity.getId());
    }

}
