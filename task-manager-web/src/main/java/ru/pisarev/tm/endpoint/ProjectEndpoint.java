package ru.pisarev.tm.endpoint;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.api.endpoint.IProjectEndpoint;
import ru.pisarev.tm.model.Project;
import ru.pisarev.tm.service.ProjectService;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.ArrayList;
import java.util.List;

@WebService
@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements IProjectEndpoint {

    @Autowired
    private ProjectService service;

    @Override
    @WebMethod
    @GetMapping("/findAll")
    public List<Project> findAll() {
        return new ArrayList<>(service.findAll());
    }

    @Override
    @WebMethod
    @GetMapping("/find/{id}")
    public Project find(@PathVariable("id") @WebParam(name = "id")final String id) {
        return service.findById(id);
    }

    @Override
    @WebMethod
    @PostMapping("/create")
    public Project create(@RequestBody @WebParam(name = "project") final Project project) {
        service.add(project);
        return project;
    }

    @Override
    @WebMethod
    @PostMapping("/createAll")
    public List<Project> createAll(@RequestBody @WebParam(name = "projects") final List<Project> projects) {
        service.addAll(projects);
        return projects;
    }

    @Override
    @WebMethod
    @PutMapping("/save")
    public Project save(@RequestBody @WebParam(name = "project") final Project project) {
        service.add(project);
        return project;
    }

    @Override
    @WebMethod
    @PutMapping("/saveAll")
    public List<Project> saveAll(@RequestBody @WebParam(name = "projects") final List<Project> projects) {
        service.addAll(projects);
        return projects;
    }

    @Override
    @WebMethod
    @DeleteMapping("/delete/{id}")
    public void delete(@PathVariable("id") @WebParam(name = "id") final String id) {
        service.removeById(id);
    }

    @Override
    @WebMethod
    @DeleteMapping("/deleteAll")
    public void deleteAll() {
        service.clear();
    }

}
