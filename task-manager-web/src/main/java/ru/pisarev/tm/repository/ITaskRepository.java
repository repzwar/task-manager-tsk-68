package ru.pisarev.tm.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.pisarev.tm.model.Task;

public interface ITaskRepository extends JpaRepository<Task, String> {


}
