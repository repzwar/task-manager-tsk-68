package ru.pisarev.tm.api.endpoint;

import org.springframework.web.bind.annotation.*;
import ru.pisarev.tm.model.Task;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@WebService
@RequestMapping("/api/tasks")
public interface ITaskEndpoint {

    @WebMethod
    @GetMapping("/findAll")
    List<Task> findAll();

    @WebMethod
    @GetMapping("/find/{id}")
    Task find(@PathVariable("id") @WebParam(name = "id") String id);

    @WebMethod
    @PostMapping("/create")
    Task create(@RequestBody @WebParam(name = "task") Task task);

    @WebMethod
    @PostMapping("/createAll")
    List<Task> createAll(@RequestBody @WebParam(name = "tasks") List<Task> tasks);

    @WebMethod
    @PostMapping("/save")
    Task save(@RequestBody  @WebParam(name = "task")Task task);

    @WebMethod
    @PostMapping("/saveAll")
    List<Task> saveAll(@RequestBody @WebParam(name = "tasks") List<Task> tasks);

    @WebMethod
    @PostMapping("/delete/{id}")
    void delete(@PathVariable("id") @WebParam(name = "id") String id);

    @WebMethod
    @PostMapping("/deleteAll")
    void deleteAll();

}
